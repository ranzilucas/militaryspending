class Country:
    """
    Abstraction for country row
    """
    name: str
    code: str
    region_type: str
    indicator: str
    # start in 1950 to 2018
    __expenditures: []
    expenditures_without_zero: []
    average_sanitized = 0.0

    def __init__(self, name, code, r_type, indicator, spends):
        self.name = name
        self.code = code
        self.region_type = r_type
        self.indicator = indicator
        self.__expenditures = []
        self.expenditures_without_zero = []
        for spend in spends:
            try:
                self.__expenditures.append(float(spend))
                self.expenditures_without_zero.append(float(spend))
            except ValueError:
                self.__expenditures.append(float(0))

    def total_average(self) -> float:
        """
            Total average
        :param: self
        :return: the total average
        :rtype: float
        """
        if self.average_sanitized < 1:
            len_spends = len(self.expenditures_without_zero)
            if len_spends > 0:
                self.average_sanitized = sum(self.expenditures_without_zero) / len_spends
        return self.average_sanitized

    def average(self, last: int) -> float:
        """
            Calculate the average
        :param last: the number of years for the calculation
        :type last: int
        :return: the average
        :rtype: float
        """
        aux = self.__expenditures[-last::1]
        return sum(aux) / len(aux)

    def get_expenditures(self, last: int = 1) -> []:
        """
            Get the expenditures list, by default the list contains only the last year
        :param last: the number of the years with contains in the list
        :type last: int
        :return: the list of spends
        :rtype: list of float
        """
        return self.__expenditures[-last::1]
