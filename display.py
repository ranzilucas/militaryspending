from typing import List

from country import Country
from equations import Equations
from matplotlib_adapter import PlotAdapter

HOW_MANY_YEARS = "How many years starting in 2018 [7]:"

MENU_MAIN = """
Statistics 
1 - Single country 
2 - Group of countries
0 - Exit
Please choose the type:"""

MENU_MULTIPLE_COUNTRIES_GROUP = """ 
Options
1 - top 5 max average expenditures 
2 - top 5 min average expenditures
3 - choose your countries
0 - return to main menu
"""

MENU_MULTIPLE_COUNTRIES_OPTIONS = """
Options:
1 - show the correlation between selected countries
2 - Plot pie chart with average
3 - Plot line graph with countries
4 - Plot line graph with simple moving average indicator
5 - Plot line graph with William Alligator indicator
0 - Return to choose again the countries
"""

HOW_MANY_COUNTRIES = "How many countries do you want choose [5]:"
CHOOSE_COUNTRY = "Choose the country using the index number or 0 to return main menu:"
LINE_GRAPH = "Do you want to print a line graph [y/N]:"
COUNTRIES_SELECTED = "Countries Selected:"
OPTION_NOT_AVAILABLE = "Option not available."
SOMETHING_WRONG = "Something went wrong, try again."


class Display:
    """
    Show the menu for the user interact with the statistics
    """
    __billion = 1_000_000_000

    def __init__(self, countries: []):
        countries.sort(key=lambda c: c.total_average())
        self.__countries = countries

    @staticmethod
    def __select_number_countries():
        """
        ask the user to select the number of countries
        """
        try:
            number_of_countries = int(input(HOW_MANY_COUNTRIES))
            if number_of_countries < 2 or number_of_countries > 5:
                number_of_countries = 5
        except ValueError:
            number_of_countries = 5
            print(OPTION_NOT_AVAILABLE)
        return number_of_countries

    @staticmethod
    def __display_countries(a_list: List[Country], header: str = "Countries available:", only_name: bool = False):
        """
        Display a list the countries in two different formats
        :param a_list: a list with countries
        :type a_list: List[Country]
        :param header: header for the list of countries
        :type header: str
        :param only_name: by default false
        :type only_name: bool
        """
        if header is not None:
            print(header)
        if only_name:
            print(', '.join(str(_.name) for _ in a_list))
        else:
            for i, _ in enumerate(a_list, start=1):
                print(f"{i:<3} - {_.name:<25}  average {_.total_average():.2f}")
        print("---------------------------------")

    @staticmethod
    def __plot_linear_chart(a_list):
        """
        Plot the default line graph
        :param a_list: a list with countries
        :type a_list: List[Country]
        """
        if "N" == input(LINE_GRAPH).upper():
            pass
        else:
            try:
                years = int(input(HOW_MANY_YEARS))
            except ValueError:
                years = 7
            PlotAdapter.line_plot_year_to_amount(a_list, years)

    def __choose_country(self):
        """
        Display option for choose the country
        :return: Return the country or None with typed the return number
        :rtype: Country
        """
        while True:
            try:
                index_c = int(input(CHOOSE_COUNTRY))
                if index_c == 0:
                    return None
                return self.__countries[index_c - 1]
            except ValueError:
                print(SOMETHING_WRONG)

    def __one_country(self):
        """
        Display option for one country
        """
        self.__display_countries(self.__countries)

        while True:
            c_selected = self.__choose_country()
            if c_selected is None:
                return

            aux_average = c_selected.total_average()
            aux_max = max(c_selected.expenditures_without_zero)
            aux_min = min(c_selected.expenditures_without_zero)
            aux_dev = Equations.standard_deviation(c_selected.expenditures_without_zero)

            print(f"""Country selected: {c_selected.name}
Average    (USD): {aux_average:12.2f} ({aux_average / self.__billion :.2f} Billions) 
Max        (USD): {aux_max:12.2f} ({aux_max / self.__billion :.3f} Billions)
Min        (USD): {aux_min:12.2f} ({aux_min / self.__billion :.3f} Billions)
Deviation  (USD): {aux_dev :12.2f} ({aux_dev / self.__billion :.3f} Billions)""")
            self.__plot_linear_chart([c_selected])

    def __menu_choose_countries(self) -> []:
        """
        Private method to select the group of countries or None when user want to return to menu
        :return: a list of countries or None
        :rtype: List or None
        """
        while True:
            a_list = []
            try:
                command = int(input(MENU_MULTIPLE_COUNTRIES_GROUP))
                if command == 0:
                    return None
                elif command == 1:
                    # select the top 5
                    a_list = self.__countries[-5:]
                    self.__display_countries(a_list, COUNTRIES_SELECTED, True)
                    return a_list
                elif command == 2:
                    # selected the less 5
                    a_list = self.__countries[0:5]
                    self.__display_countries(a_list, COUNTRIES_SELECTED, True)
                    return a_list
                elif command == 3:
                    self.__display_countries(self.__countries)

                    for i in range(self.__select_number_countries()):
                        aux = self.__choose_country()
                        if aux is None:
                            return None
                        a_list.append(aux)

                    self.__display_countries(a_list, COUNTRIES_SELECTED, True)
                    return a_list
                else:
                    print(OPTION_NOT_AVAILABLE)
            except ValueError:
                print(SOMETHING_WRONG)

    def __multiple_countries(self):
        """
        Display multiple countries options
        """
        while True:
            try:
                a_list = self.__menu_choose_countries()
                if a_list is None:
                    return

                while True:
                    command = input(MENU_MULTIPLE_COUNTRIES_OPTIONS)
                    if command == "0":
                        break
                    elif command == "1":
                        for i in range(len(a_list)):
                            for j in range(i + 1, len(a_list)):
                                print(f"Correlation {a_list[i].name} x {a_list[j].name} ="
                                      f" {Equations.correlation(a_list[i].expenditures_without_zero, a_list[j].expenditures_without_zero):.2f}")
                    elif command == "2":
                        # chart of pie to show the average
                        index_name = []
                        index_average = []
                        for _ in a_list:
                            index_name.append(_.name)
                            index_average.append(_.total_average())
                        PlotAdapter.pie_chart(index_name, index_average)
                    elif command == "3":
                        self.__plot_linear_chart(a_list)
                    elif command == "4":
                        self.__plot_line_with_sma(a_list)
                    elif command == "5":
                        self.__plot_line_with_alligator(a_list)
                    else:
                        print(OPTION_NOT_AVAILABLE)
            except ValueError:
                print(SOMETHING_WRONG)

    @staticmethod
    def __plot_line_with_sma(a_list: List[Country]):
        """
        Plot line graph with simple moving average
        :param a_list: list of countries
        :type a_list: list
        """
        try:
            years = int(input(HOW_MANY_YEARS))
        except ValueError:
            years = 7

        in_billion = "N" != input("Do you want plot in billions [y/N]:").upper()
        name_list = []
        exp_list = []
        for c in a_list:
            name_list.append(c.name)
            aux = c.get_expenditures(years)
            exp_list.append(aux[::-1])
        sma_list = Equations.simple_moving_average(exp_list)
        PlotAdapter.line_plot_with_simple_moving_average(name_list, exp_list, sma_list, in_billion)

    @staticmethod
    def __plot_line_with_alligator(a_list: List[Country]):
        """
        Plot the graph showing the William Alligator indicator
        :param a_list: list of countries
        :type a_list: list
        """
        try:
            years = int(input("How many years starting in 2018 (min 13) [15]:"))
            if 13 > years or 50 < years:
                years = 15
        except ValueError:
            years = 15

        name_list = []
        expend_list = []
        for c in a_list:
            name_list.append(c.name)
            aux = c.get_expenditures(years)
            expend_list.append(aux)
        sma_list = Equations.simple_moving_average(expend_list)

        jaw = Equations.smoothed_moving_average(sma_list, 13, 8)
        teeth = Equations.smoothed_moving_average(sma_list, 8, 5)
        lips = Equations.smoothed_moving_average(sma_list, 5, 2)

        expend_list.append(lips)
        expend_list.append(teeth)
        expend_list.append(jaw)

        expend_list = Equations.complete_with_none(expend_list)
        PlotAdapter.line_plot_alligator(name_list, expend_list[:-3], jaw, teeth, lips)

    def main_menu(self):
        """
        Main menu with contains the statistics
        """
        while True:
            command = input(MENU_MAIN)
            if command == "0":
                print("See you soon!")
                exit(1)
            elif command == "1":
                self.__one_country()
            elif command == "2":
                self.__multiple_countries()
            else:
                print(SOMETHING_WRONG)
