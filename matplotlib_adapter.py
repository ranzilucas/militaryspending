import matplotlib.pyplot as plt

BILLION = 1_000_000_000


class PlotAdapter:
    """
        Class responsible to adapter the information and generate the graphs
    """

    @staticmethod
    def line_plot_with_simple_moving_average(names_list: [], a_list: [[]], sma: [], billions: bool = False):
        """
            Plot the linear graph using the list and the names, show the average (SMA)
        :param names_list: list which names of to show in the graph
        :type names_list: list
        :param a_list: list with contains other list of data
        :type a_list: list of lists
        :param sma: contains a list of simple moving average
        :type sma: list
        :param billions: when true will show the data in billions by default false
        :type billions: bool
        """
        years = list(map(lambda year: 2018 - year, range(len(sma))))

        fig, ax = plt.subplots()

        for i, spends in enumerate(a_list):
            if billions:
                spends = list(map(lambda spend: spend / BILLION, spends))
            ax.plot(years, spends, label=names_list[i])

        if billions:
            sma = list(map(lambda spend: spend / BILLION, sma))
        ax.plot(years, sma, label="SMA", color="black")

        plt.xlabel("Years")

        if billions:
            plt.ylabel("Expend in USD in Billions")
        else:
            plt.ylabel("Expend in USD")

        ax.legend()
        ax.grid(True)
        plt.show()

    @staticmethod
    def line_plot_alligator(names_list: [], a_list: [[]], jaw: [], teeth: [], lips: []):
        """
            Plot the linear graph using the William Alligator indicator
        :param lips: smma equation to lips
        :type lips: List
        :param teeth: smma equation to teeth
        :type teeth: List
        :param jaw: smma equation to jaw
        :type jaw: List
        :param names_list: list which names of to show in the graph
        :type names_list: list
        :param a_list: list with contains other list of data
        :type a_list: list of lists
        """
        years = list(map(lambda year: 2028 - year, range(len(lips))))
        years = years[::-1]
        fig, ax = plt.subplots()

        for i, spends in enumerate(a_list):
            ax.plot(years, spends, label=names_list[i])

        ax.plot(years, lips, color='green')
        ax.plot(years, teeth, color='red')
        ax.plot(years, jaw, color='blue')

        plt.xlabel("Years")
        plt.ylabel("Expend in USD")

        ax.legend()
        ax.grid(True)
        plt.show()

    @staticmethod
    def pie_chart(labels, values, explode_index: bool = False):
        """
        Pie chart
        :param labels:
        :type labels:
        :param values:
        :type values:
        :param explode_index: select one data in the chart
        :type explode_index: bool
        """
        fig1, ax1 = plt.subplots()
        if explode_index:
            explode = []
            for l in labels:
                if explode_index == l:
                    explode.append(0.1)
                else:
                    explode.append(0)
            ax1.pie(values, explode=explode, labels=labels, autopct='%1.1f%%',
                    shadow=True, startangle=90)
        else:
            ax1.pie(values, labels=labels, autopct='%1.1f%%', shadow=True, startangle=90)

        ax1.axis('equal')
        plt.show()

    @staticmethod
    def line_plot_year_to_amount(list_countries, years_total, in_billion: bool = False):
        """
        Line plot year to amount
        :param list_countries: list of countries
        :type list_countries: List
        :param years_total: total the year to print in the chart initialize in 2018
        :type years_total: int
        :param in_billion: show in billions
        :type in_billion: bool
        """
        years = list(map(lambda y: 2018 - y, range(years_total)))
        years = years[::-1]

        fig, ax = plt.subplots()

        for country in list_countries:
            if in_billion:
                ax.plot(years, list(map(lambda spend: spend / BILLION, country.get_expenditures(years_total))),
                        label=country.name)
            else:
                ax.plot(years, country.get_expenditures(years_total), label=country.name)

        plt.xlabel("Years")
        if in_billion:
            plt.ylabel("Expenses in USD in Billions")
        else:
            plt.ylabel("Expenses in USD")

        ax.legend()
        ax.grid(True)
        plt.show()
