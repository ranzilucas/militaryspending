from display import Display
from load_military_expenditure import LoadMilitaryExpenditure

if __name__ == "__main__":
    countries = LoadMilitaryExpenditure.load("Military Expenditure.csv")
    display = Display(countries)
    print("Welcome to Military Expenditure!")
    display.main_menu()
