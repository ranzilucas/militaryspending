from math import sqrt


class Equations:
    @staticmethod
    def correlation(x_list: [], y_list: []) -> float:
        """
            Show the correlations between two list of data
            1 implies a perfect positive linear relationship between the variables
            -1 implies a perfect negative linear relationship between the variables
            0 implies no relationship between the variables
        :param x_list: a list of data
        :type x_list: list of float
        :param y_list: a list of data
        :type y_list: list of float
        :return: number between -1 at 1 means the correlation
        :rtype: float
        """
        x_mean = sum(x_list) / len(x_list)
        y_mean = sum(y_list) / len(y_list)

        x_deviations = [x - x_mean for x in x_list]
        y_deviations = [y - y_mean for y in y_list]

        xy_deviations = [x * y for (x, y) in zip(x_deviations, y_deviations)]

        x_sqd_deviations = [(x - x_mean) ** 2 for x in x_list]
        y_sqd_deviations = [(y - y_mean) ** 2 for y in y_list]

        return sum(xy_deviations) / (sqrt(sum(x_sqd_deviations)) * (sqrt(sum(y_sqd_deviations))))

    @staticmethod
    def simple_moving_average(a_list: []) -> []:
        """
            When receive a list with contains other lists, this method will create a moving average.
        :param a_list: list with many list to make average
        :type a_list: list
        :return: a list with the average
        :rtype: list
        """
        _list = []
        len_list = len(a_list)
        if len_list == 1:
            return a_list[0].copy()
        elif len_list == 0:
            return []

        for i in range(len(a_list[0])):
            aux = 0
            for j in range(len_list):
                aux += a_list[j][i]
            _list.append(aux / len_list)
        return _list

    @staticmethod
    def smoothed_moving_average2(average: [], periods: int, shift: int) -> []:
        """
            Smoothed Moving Average
        :param periods: amount of periods for calculating moving average
        :type periods: int
        :param average: contains a list with the average it will be used to calculate the smoothed moving average
        :type average: list
        :param shift: contain the future shift, how many calculation it will be to the future
        :type shift: int
        :return: list of SMMAs from periods
        :rtype: list
        """
        # clone the list, for remove the reference
        a_list = average.copy()

        len_list = len(a_list)

        if len_list < periods:
            print("cannot continue because list is too short")
            return []

        for i in range(periods, len_list + shift):
            sum_spends = sum(a_list[i - periods:i])
            a_list.append(sum_spends / periods)

        return a_list[-(len_list + periods): -1]

    @staticmethod
    def smoothed_moving_average(average: [], periods: int, shift: int) -> []:
        """
            Smoothed Moving Average
        :param periods: amount of periods for calculating moving average
        :type periods: int
        :param average: contains a list with the average it will be used to calculate the smoothed moving average
        :type average: list
        :param shift: contain the future shift, how many calculation it will be to the future
        :type shift: int
        :return: list of SMMAs from periods
        :rtype: list
        """
        # clone the list, for remove the reference
        a_list_final = average[:periods].copy()
        a_list = average.copy()

        len_list = len(a_list)

        if len_list < periods:
            print("cannot continue because list is too short")
            return []

        for i in range(periods, len_list + shift):
            sum_spends = sum(a_list[i - periods:i])
            aux = sum_spends / periods
            if i >= len_list:
                a_list.append(aux)
            a_list_final.append(aux)

        return a_list_final

    @staticmethod
    def complete_with_none(_matrix: []) -> []:
        """
            Complete the lists in the list received with None, this method normalize all list to the same length
        :param _matrix: a matrix with lists inside
        :type _matrix: list
        :return: the parameter received filled with None
        :rtype: list
        """
        bigger = 0
        for i in _matrix:
            aux = len(i)
            if aux > bigger:
                bigger = aux

        for _list in _matrix:
            for j in range(len(_list), bigger):
                _list.append(None)

        return _matrix

    @staticmethod
    def standard_deviation(a_list: list) -> float:
        """
            Calculation the standard deviation using the list of values
        :param a_list: list of elements to make the standard deviation
        :type a_list: list
        :return: the deviation
        :rtype: float
        """
        mean = sum(a_list) / len(a_list)
        deviations = [(x - mean) ** 2 for x in a_list]
        return sqrt(sum(deviations) / (len(a_list) - 1))
