from typing import List

from country import Country


class LoadMilitaryExpenditure:
    """
    Class responsible to load the military expenditure
    """

    @staticmethod
    def load(file: str) -> List[Country]:
        """
        Load the file and return the list of the countries which are average
        :param file: string with the file path
        :type file: str
        :return: a list with countries
        :rtype: List
        """
        countries = []
        with open(file, "r") as militaryFile:
            for line in militaryFile.readlines()[1:]:
                columns = line.split(",")
                if columns[2] == "Country":
                    country = Country(columns[0], columns[1], columns[2], columns[3], columns[4:])
                    if country.total_average() > 0:
                        countries.append(country)

        return countries
