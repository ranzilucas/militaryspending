from unittest import TestCase

from country import Country


class TestCountry(TestCase):

    def test_total_average(self):
        country = Country("Test", "TS", "Type", "USD", ["2", "", "5", "7", "0", "10"])
        self.assertEqual(country.total_average(), 4.8)

    def test_average(self):
        country = Country("Test", "TS", "Type", "USD", ["2", "", "5", "7", "0", "10"])
        self.assertEqual(country.average(6), 4)

    def test_get_expenditures(self):
        country = Country("Test", "TS", "Type", "USD", ["2", "", "5", "7", "0", "10"])
        self.assertEqual(country.get_expenditures(2), [0.0, 10.0])
