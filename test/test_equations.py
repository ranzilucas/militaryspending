from unittest import TestCase

from equations import Equations


class TestEquations(TestCase):
    def test_correlation(self):
        x_list = [26.8, 62.4, 25.9, 9.1, 74.8, 23.4, 15.8, 44.1, 25.9, 54.9]
        y_list = [50.5, 84, 74.4, 71.2, 85.3, 41.7, 61.1, 71.4, 45.9, 90.4]
        self.assertEqual(0.6739125637228505, Equations.correlation(x_list, y_list))

    def test_simple_moving_average(self):
        average = Equations.simple_moving_average([[1, 2, 3, 4, 5], [5, 4, 3, 2, 1]])
        self.assertEqual([3, 3, 3, 3, 3], average)

    def test_simple_moving_average_one_list(self):
        average = Equations.simple_moving_average([[1, 2, 3, 4, 5]])
        self.assertEqual([1, 2, 3, 4, 5], average)

    def test_simple_moving_average_empty_list(self):
        average = Equations.simple_moving_average([])
        self.assertEqual([], average)

    def test_smoothed_moving_average2(self):
        a_list = [1, 2, 3, 4, 5, 6]
        self.assertEqual(Equations.smoothed_moving_average2(a_list, 5, 2), [1, 2, 3, 4, 5, 6, 3.0, 4.0])
        self.assertEqual(Equations.smoothed_moving_average2(a_list, 5, 3), [1, 2, 3, 4, 5, 6, 3.0, 4.0, 4.2])

        self.assertEqual(Equations.smoothed_moving_average2([], 1, 1), [])

    def test_smoothed_moving_average(self):
        average = [1, 2, 3, 4, 5, 6]
        self.assertEqual(
            (Equations.smoothed_moving_average(average, 2, 3), [1, 2, 1.5, 2.5, 3.5, 4.5, 5.5, 5.75, 5.625]))

    def test_complete_with_none(self):
        _matrix = [[1, 2], [3, 6, 8]]
        self.assertEqual([[1, 2, None], [3, 6, 8]], Equations.complete_with_none(_matrix))

    def test_complete_with_none_equals(self):
        _matrix = [[1, 2], [3, 6]]
        self.assertEqual([[1, 2], [3, 6]], Equations.complete_with_none(_matrix))

    def test_complete_with_none_third_list(self):
        _matrix = [[1, 2], [3, 6, 8], [5]]
        self.assertEqual([[1, 2, None], [3, 6, 8], [5, None, None]], Equations.complete_with_none(_matrix))

    def test_standard_deviation(self):
        expenditures = [2, 4, 8, 8]
        self.assertEqual(3, Equations.standard_deviation(expenditures))
